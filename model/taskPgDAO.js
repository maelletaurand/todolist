const { Client } = require('pg');
const Task = require('../model/task');


class TaskPgDAO{

    constructor(){
    }

    getAllTasks(displaycb){

        let client = new Client({
            connectionString : 'postgres://postgres:Password1@192.168.1.50:5432/todo'
        });

        /* autre syntaxe
        let client = new Client({

            user: 'postgres',
            host: '192.168.1.50',
            database: 'todo',
            password: 'Password1',
            port: 5432,
        })
        */


        client.connect(function (err){
            if (err) return done(err);
        });

        const query = {
            name: 'fetch-all-task',
            text: 'SELECT * FROM task',
        };

        client.query(query, function(err, result){
            let listeTaches = [];
            if (err) {
                console.log(err.stack);
            } else {
                result.rows.forEach(function(row) {
                    let uneTask;

                    uneTask = new Task(row['id'], row['libelle']);
                    listeTaches.push(uneTask);
                });
                displaycb(listeTaches);
            }
            client.end();
        });




    };

}


module.exports = TaskPgDAO;