CREATE FUNCTION get_nb_taches () RETURNS integer AS
$$
DECLARE
nb integer;
BEGIN
SELECT INTO nb COUNT(*) FROM task;
RETURN nb;
END ;
$$
LANGUAGE 'plpgsql';