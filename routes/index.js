var express = require('express');
var router = express.Router();

var controller=require('../controllers/controller')

router.get('/',controller.index);


// GET toutes les tâches
router.get('/taches',controller.getLesTaches);

// GET la tâche id
router.get('/taches/:id',controller.getOneTache);

//POST ajout d'une tâche
router.post('/taches',controller.addOneTache);

module.exports = router;
