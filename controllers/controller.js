const pg = require('pg');
const TaskPgDAO = require('../model/taskPgDAO');


/* GET home page. */
exports.index= function(req, res, next) {
    const connectionString = 'postgres://postgres:Password1@192.168.1.50:5432/todo';

    const db = new pg.Client(connectionString);
    db.connect();


    const query = {
        name: 'fetch-one-task',
        text: 'SELECT get_nb_taches()',
    };

    db.query(query,
        function(err, result){
            if (err) {
                console.log(err.stack);
                res.send('ERROR');

            } else {
                //console.log(result.rows);
                //console.log(result.rows[0].get_nb_taches);
                res.render('index', { title: 'todoList',nbTaches:result.rows[0].get_nb_taches});

            }
            db.end();
        }
    );

};

const taskPgDAO = new TaskPgDAO();

/*GET Liste des tâches*/
exports.getLesTaches = function(req, res,next){

    taskPgDAO.getAllTasks(
        function(lesTaches){

            res.render('taches',{listeDesTaches: lesTaches})
        }
    );
};

/*GET uneTache */
exports.getOneTache=function(req, res, next) {
    const connectionString = 'postgres://postgres:Password1@192.168.1.50:5432/todo';

    const db = new pg.Client(connectionString);
    db.connect();


    const query = {
        name: 'fetch-one-task',
        text: 'SELECT * FROM task WHERE id = $1',
        values: [req.params.id]
    };

    db.query(query,
        function(err, result){
            if (err) {
                console.log(err.stack);
                res.send('ERROR');

            } else {
                //res.render('taches', { listeDesTaches : result });
                res.send('Détails de la tache n°' + req.params.id + ': Libelle = '+ result.rows[0]['libelle']);
            }
            db.end();
        }
    );
};

exports.addOneTache=function(req,res,next){

    const connectionString = 'postgres://postgres:Password1@192.168.1.50:5432/todo';

    const db = new pg.Client(connectionString);
    db.connect();

    console.log("Nouvelle tache à ajouter:" + req.body.libelle);

    const query = {
        name: 'insert-one-task',
        text: 'INSERT INTO task(libelle) VALUES($1) RETURNING *',
        values: [req.body.libelle]
    };


    db.query(query,
        function(err, result){
            if (err) {
                console.log(err.stack)
            } else {
                console.log(result.rows[0]);

                res.redirect('/taches');
            }
            db.end();
        }
    );

};